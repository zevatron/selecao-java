package com.indracompany.desafioIndra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.indracompany.desafioIndra.upload.StorageProperties;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class DesafioIndraApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioIndraApplication.class, args);
	}
	

}
