package com.indracompany.desafioIndra.repository;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.indracompany.desafioIndra.model.Historico;

@RepositoryRestResource(collectionResourceRel = "historicos", path = "historicos")
public interface HistoricoRepository extends PagingAndSortingRepository<Historico, Long>{
	
	Historico findByCodigo(@Param("codigoRevenda")int codigo);
	
	@Query("SELECT new map(AVG(h.valorVenda) as mediaMunicipio) FROM Historico h WHERE h.municipio = UPPER(:municipio) " )
	public Map<String,BigDecimal> mediaValorVendaMunicipio(@Param("municipio") String municipio);
	
	@Query("SELECT h FROM Historico h WHERE h.regiao = UPPER(:regiao) ")
	public List<Historico> porRegiao(@Param("regiao") String regiao , Pageable pageable);
	
	

}
