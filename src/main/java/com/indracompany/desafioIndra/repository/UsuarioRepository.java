package com.indracompany.desafioIndra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.desafioIndra.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	
	Usuario findByEmail(String email);

}
