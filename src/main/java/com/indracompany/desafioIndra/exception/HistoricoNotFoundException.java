package com.indracompany.desafioIndra.exception;

public class HistoricoNotFoundException extends RuntimeException{

	public HistoricoNotFoundException(Long id) {
		
		super("Não foi possível encontrar o histórico " + id);
	}
	
}
