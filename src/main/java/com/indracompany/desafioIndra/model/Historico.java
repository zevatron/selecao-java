package com.indracompany.desafioIndra.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
public class Historico {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String revenda;
	
	private Integer codigo;
	
	private String regiao;
	
	private String estado;
	
	private String municipio;
	
	private String produto;
	
	@Temporal(TemporalType.DATE)
	private Date dataColeta;
	
	private BigDecimal valorCompra;
	
	private BigDecimal valorVenda;
	
	private String unidadeMedida;
	
	private String bandeira;
}
