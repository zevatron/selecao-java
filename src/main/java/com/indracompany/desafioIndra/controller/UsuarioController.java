package com.indracompany.desafioIndra.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.desafioIndra.exception.UsuarioNotFoundException;
import com.indracompany.desafioIndra.model.Usuario;
import com.indracompany.desafioIndra.repository.UsuarioRepository;

@RestController
@RequestMapping(value = "/usuarios")
public class UsuarioController {

	
	private final UsuarioRepository usuarioRepository;
	
	public UsuarioController(UsuarioRepository usuarioRepository) {
		
		this.usuarioRepository = usuarioRepository;
	}

	@GetMapping
	public List<Usuario> listar() {

		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	@PostMapping
	Usuario novoUsusario(@RequestBody Usuario novoUsuario) {
		return usuarioRepository.save(novoUsuario);
	}

	@GetMapping("/{id}")
	public Usuario buscarPorId(@PathVariable Long id) {
		
		return usuarioRepository.findById(id)
				.orElseThrow( ()-> new UsuarioNotFoundException(id) );

	}
	
	@PutMapping("/{id}")
	Usuario atualizaUsuario(@RequestBody Usuario novoUsuario, @PathVariable Long id) {

		return usuarioRepository.findById(id)
			.map(usuario -> {
				usuario.setNome(novoUsuario.getNome());
				usuario.setEmail(novoUsuario.getEmail());
				usuario.setSenha(novoUsuario.getSenha());
				usuario.setAdmin(novoUsuario.isAdmin());
				return usuarioRepository.save(usuario);
			})
			.orElseThrow( ()-> new UsuarioNotFoundException(id) );
	}

	@DeleteMapping("/{id}")
	void apagaUsuario(@PathVariable Long id) {
		usuarioRepository.deleteById(id);
	}
	

}
