package com.indracompany.desafioIndra.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.indracompany.desafioIndra.exception.HistoricoNotFoundException;
import com.indracompany.desafioIndra.model.Historico;
import com.indracompany.desafioIndra.repository.HistoricoRepository;

//@RestController //Estou usando o @RepositoryRestResource do historicoRepository
@RequestMapping(value = "/historicos")
public class HistoricoController {

	
	private final HistoricoRepository historicoRepository;
	
	public HistoricoController(HistoricoRepository historicoRepository) {
		
		this.historicoRepository = historicoRepository;
	}

	@GetMapping
	public List<Historico> listar() {

		return (List<Historico>) historicoRepository.findAll();
	}
	
	@PostMapping
	Historico novoHistorico(@RequestBody Historico novoHistorico) {
		return historicoRepository.save(novoHistorico);
	}

	@GetMapping("/{id}")
	public Historico buscarPorId(@PathVariable Long id) {
		
		return historicoRepository.findById(id)
				.orElseThrow( ()-> new HistoricoNotFoundException(id) );

	}
	
	@PutMapping("/{id}")
	Historico atualizaHistorico(@RequestBody Historico novoHistorico, @PathVariable Long id) {

		return historicoRepository.findById(id)
			.map(historico -> {
				historico.setBandeira(novoHistorico.getBandeira());
				historico.setCodigo(novoHistorico.getCodigo());
				historico.setDataColeta(novoHistorico.getDataColeta());
				historico.setEstado(novoHistorico.getEstado());
				historico.setMunicipio(novoHistorico.getMunicipio());
				historico.setProduto(novoHistorico.getProduto());
				historico.setRegiao(novoHistorico.getRegiao());
				historico.setRevenda(novoHistorico.getUnidadeMedida());
				historico.setUnidadeMedida(novoHistorico.getUnidadeMedida());
				historico.setValorCompra(novoHistorico.getValorCompra());
				historico.setValorVenda(novoHistorico.getValorVenda());
				return historicoRepository.save(historico);
			})
			.orElseThrow( () -> new HistoricoNotFoundException(id));
	}

	@DeleteMapping("/{id}")
	void apagaHistorico(@PathVariable Long id) {
		historicoRepository.deleteById(id);
	}
	

}