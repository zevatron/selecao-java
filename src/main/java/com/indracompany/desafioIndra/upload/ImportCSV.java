package com.indracompany.desafioIndra.upload;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.indracompany.desafioIndra.model.Historico;
import com.indracompany.desafioIndra.repository.HistoricoRepository;

public class ImportCSV implements Runnable {
	
	String[] dados;
	int lineNumber;
	HistoricoRepository historicoRepository;
	
	
	public ImportCSV(String[] dados, int lineNumber,
			HistoricoRepository historicoRepository) {
		this.dados = dados;
		this.lineNumber = lineNumber;
		this.historicoRepository = historicoRepository;
	}
	
	@Override
	public void run() {
		Historico historico = new Historico();
		
		if(lineNumber != 1 && dados.length == 11) { //REMOVE A PRIMEIRA LINHA DO CABEÇALHO E AS LINHAS COM DADOS INCONSISTENTES
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date dataColeta = new Date();
			try {
				dataColeta = formatter.parse(dados[6]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			Revenda revenda = revendaRepository.findByCodigo(Integer.parseInt(dados[4]));
//			if (revenda == null) {
//				revenda = gravarRevenda(revenda, revendaRepository);
//			}
			historico.setCodigo(Integer.parseInt(dados[4]));
			historico.setRegiao(dados[0].trim());
			historico.setEstado(dados[1].trim());
			historico.setMunicipio(dados[2].trim());
			historico.setRevenda(dados[3].trim());
			historico.setProduto(dados[5].trim());
			historico.setDataColeta(dataColeta);
			historico.setValorCompra(dados[7].isEmpty() || dados[7] == null ? null : new BigDecimal(dados[7].replace(',', '.')));
			historico.setValorVenda(new BigDecimal(dados[8].isEmpty() || dados[8] == null ? null :   dados[8].replace(',', '.')));
			historico.setUnidadeMedida(dados[9].trim());
			historico.setBandeira(dados[10].trim());
			
			historicoRepository.save(historico);
		}
	}
	
	

	
}
